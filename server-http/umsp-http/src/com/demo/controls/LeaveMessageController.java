package com.demo.controls;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import com.demo.common.model.Leavemessage;
import com.jfinal.plugin.activerecord.Page;

/**
 * IndexController
 */
public class LeaveMessageController extends BaseController {
	public void index() {
		int pageindex = getParaToInt("pageindex",1);
		int pageSize= getParaToInt("pagesize",10);
		Page<Leavemessage> users = Leavemessage.dao.paginate(pageindex, pageSize,"select * ", " from leavemessage where id  > ? order by id desc","0");
//		Page<Leavemessage> users = Leavemessage.dao.paginate(pageindex, pageSize,"select * from leavemessage where isreplay = 0");
		renderJson(users.getList());
	}
	public void add() {
		int userid;
		if ((userid = checkToken()) < 0) {
			return;
		}
		try {
			String username;
			username = URLDecoder.decode(getPara("username"),"UTF-8");
			if(isEmpty(username)){renderErr("username is null") ;return;}
			String useravtor = getPara("useravator");
			if(isEmpty(useravtor)){renderErr("useravator is null") ;return;}
			String context = URLDecoder.decode(getPara("context"),"UTF-8");
			if(isEmpty(context)){renderErr("context is null") ;return;}
			Leavemessage bean = new Leavemessage();
			bean.setUsername(username);
			bean.setUseravator(useravtor);
			bean.setIsanonymous(getParaToInt("isanonymous",0));
			bean.setCreatetime(System.currentTimeMillis());
			bean.setUserid(userid);
			bean.setIsreplay(0);
			bean.setReplaycount(0);
			bean.setReplaymessageid(0);
			bean.setIsdelete(0);
			bean.setLikecount(0);
			boolean isreplay = getParaToInt("isreplay",0)==TRUE;
			Leavemessage msg=null;
			if (isreplay) {
				bean.setIsreplay(1);
				int replayid = getParaToInt("replaymessageid",0);
				if(replayid==0){renderErr("replaymessageid is null");return;}
				bean.setReplaymessageid(replayid);
				msg = Leavemessage.dao.findById(replayid);
				if(msg==null){renderErr("Not found the replaymessageid ");return;}
				msg.setReplaycount(msg.getReplaycount()+1);
				msg.update();
				String msgcontext = msg.getContext();
				context = "replay "+msg.getUsername()+" ".concat(msgcontext.substring(0,msgcontext.length()>10?10:(msgcontext.length()-1))).concat("... \n").concat(context);
				MsgController.add(MSGTYPE_LEAVEMSG, msg.getUserid(), "有人回复了您的留言", userid, replayid);
			}
			bean.setContext(context);
			bean.setUserid(userid);
			bean.save();
			renderOK();
			return ;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderErr(" err" +e.getMessage()) ;return;
		}
		

	}
	public void remove() {
		
		if ((checkToken()) < 0) {
			return;
		}
		int id = getParaToInt("id",0);
		if(isEmpty(id)){renderErr("id is null");return;}
		Leavemessage msg = Leavemessage.dao.findById(id);
		if(msg==null){renderErr("Not found the messageid ");return;}
		if(msg.delete()){
			renderOK();
			return ;
		}else{
			renderErr("remove fail ");
		}
	}
	public void like() {
		
		int userid = checkToken();
		if (userid < 0) {
			return;
		}
		int id = getParaToInt("id",0);
		if(isEmpty(id)){renderErr("id is null");return;}
		Leavemessage msg = Leavemessage.dao.findById(id);
		if(msg==null){renderErr("Not found the messageid ");return;}
		msg.setLikecount(msg.getLikecount()+1);
		if(msg.update()){
			renderOK();
			MsgController.add(MSGTYPE_LEAVEMSG, msg.getUserid(), "有人赞了您的留言", userid, msg.getId());
			return ;
		}else{
			renderErr("like fail ,id is "+id);
		}
	}
}