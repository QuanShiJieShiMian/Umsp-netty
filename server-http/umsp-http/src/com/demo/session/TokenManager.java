package com.demo.session;

import java.util.HashMap;

public class TokenManager {
	private static TokenManager instance = new TokenManager();
	private static HashMap<String, Integer> tokenMap = new HashMap<String, Integer>();
	public int getIDByToken(String token){
		System.out.println("get id by token :"+token);
		return tokenMap.containsKey(token)?tokenMap.get(token).intValue():-1;
	}
	public static TokenManager getInstance(){
		return instance ;
	}
	public void add(String token, Integer id) {
		tokenMap.put(token, id);
	}
}
