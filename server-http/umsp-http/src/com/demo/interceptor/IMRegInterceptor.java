package com.demo.interceptor;

import org.apache.log4j.Logger;

import com.demo.common.model.User;
import com.easemob.server.example.httpclient.apidemo.EasemobIMUsers;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

public class IMRegInterceptor implements Interceptor {
	private static final Logger LOG = Logger.getLogger(IMRegInterceptor.class);

	public void intercept(Invocation inv) {
		LOG.info("Before invoking " + inv.getActionKey());
		inv.invoke();
		Object retrunValue = inv.getReturnValue();
		if (retrunValue != null) {
			User user = (User) retrunValue;
			registIM(user);
		}

		LOG.info("After invoking " + inv.getActionKey());
	}

	public static boolean registIM(User user) {
		LOG.info("regitIM result success:" + user.getImusername()+" pw:"+
				user.getImpassword());
		ObjectNode jo = EasemobIMUsers.regitIM(user.getImusername(),
				user.getImpassword());
		if (jo != null) {
			LOG.info("regitIM result success:" + jo);
			return true;
		}
		return false;
	}
}