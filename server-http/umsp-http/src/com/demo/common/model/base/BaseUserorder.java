package com.demo.common.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseUserorder<M extends BaseUserorder<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}

	public java.lang.Integer getId() {
		return get("id");
	}

	public void setOrdertype(java.lang.Integer ordertype) {
		set("ordertype", ordertype);
	}

	public java.lang.Integer getOrdertype() {
		return get("ordertype");
	}

	public void setFuserid(java.lang.Integer fuserid) {
		set("fuserid", fuserid);
	}

	public java.lang.Integer getFuserid() {
		return get("fuserid");
	}

	public void setCreatetime(java.util.Date createtime) {
		set("createtime", createtime);
	}

	public java.util.Date getCreatetime() {
		return get("createtime");
	}

	public void setEndtime(java.util.Date endtime) {
		set("endtime", endtime);
	}

	public java.util.Date getEndtime() {
		return get("endtime");
	}

	public void setIspay(java.lang.Integer ispay) {
		set("ispay", ispay);
	}

	public java.lang.Integer getIspay() {
		return get("ispay");
	}

	public void setPaytype(java.lang.Integer paytype) {
		set("paytype", paytype);
	}

	public java.lang.Integer getPaytype() {
		return get("paytype");
	}

	public void setTotalprice(java.lang.Integer totalprice) {
		set("totalprice", totalprice);
	}

	public java.lang.Integer getTotalprice() {
		return get("totalprice");
	}

	public void setPlaceid(java.lang.Integer placeid) {
		set("placeid", placeid);
	}

	public java.lang.Integer getPlaceid() {
		return get("placeid");
	}

	public void setSendername(java.lang.String sendername) {
		set("sendername", sendername);
	}

	public java.lang.String getSendername() {
		return get("sendername");
	}

	public void setSenderphone(java.lang.String senderphone) {
		set("senderphone", senderphone);
	}

	public java.lang.String getSenderphone() {
		return get("senderphone");
	}

	public void setPostmanname(java.lang.String postmanname) {
		set("postmanname", postmanname);
	}

	public java.lang.String getPostmanname() {
		return get("postmanname");
	}

	public void setPostmanphone(java.lang.String postmanphone) {
		set("postmanphone", postmanphone);
	}

	public java.lang.String getPostmanphone() {
		return get("postmanphone");
	}

	public void setDispatchtime(java.util.Date dispatchtime) {
		set("dispatchtime", dispatchtime);
	}

	public java.util.Date getDispatchtime() {
		return get("dispatchtime");
	}

}
