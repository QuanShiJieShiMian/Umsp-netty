package com.demo.common.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseUser<M extends BaseUser<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}

	public java.lang.Integer getId() {
		return get("id");
	}

	public void setUsername(java.lang.String username) {
		set("username", username);
	}

	public java.lang.String getUsername() {
		return get("username");
	}

	public void setToken(java.lang.String token) {
		set("token", token);
	}

	public java.lang.String getToken() {
		return get("token");
	}

	public void setPassword(java.lang.String password) {
		set("password", password);
	}

	public java.lang.String getPassword() {
		return get("password");
	}

	public void setPaypassword(java.lang.String paypassword) {
		set("paypassword", paypassword);
	}

	public java.lang.String getPaypassword() {
		return get("paypassword");
	}

	public void setNickname(java.lang.String nickname) {
		set("nickname", nickname);
	}

	public java.lang.String getNickname() {
		return get("nickname");
	}

	public void setTel(java.lang.String tel) {
		set("tel", tel);
	}

	public java.lang.String getTel() {
		return get("tel");
	}

	public void setAvatar(java.lang.String avatar) {
		set("avatar", avatar);
	}

	public java.lang.String getAvatar() {
		return get("avatar");
	}

	public void setAvatarhd(java.lang.String avatarhd) {
		set("avatarhd", avatarhd);
	}

	public java.lang.String getAvatarhd() {
		return get("avatarhd");
	}

	public void setMemberpoints(java.lang.Integer memberpoints) {
		set("memberpoints", memberpoints);
	}

	public java.lang.Integer getMemberpoints() {
		return get("memberpoints");
	}

	public void setMemberpointsbalance(java.lang.Integer memberpointsbalance) {
		set("memberpointsbalance", memberpointsbalance);
	}

	public java.lang.Integer getMemberpointsbalance() {
		return get("memberpointsbalance");
	}

	public void setBanlance(java.lang.Integer banlance) {
		set("banlance", banlance);
	}

	public java.lang.Integer getBanlance() {
		return get("banlance");
	}

	public void setSacncode(java.lang.String sacncode) {
		set("sacncode", sacncode);
	}

	public java.lang.String getSacncode() {
		return get("sacncode");
	}

	public void setSacncodeimage(java.lang.String sacncodeimage) {
		set("sacncodeimage", sacncodeimage);
	}

	public java.lang.String getSacncodeimage() {
		return get("sacncodeimage");
	}

	public void setThirdid(java.lang.String thirdid) {
		set("thirdid", thirdid);
	}

	public java.lang.String getThirdid() {
		return get("thirdid");
	}

	public void setThirdtoken(java.lang.String thirdtoken) {
		set("thirdtoken", thirdtoken);
	}

	public java.lang.String getThirdtoken() {
		return get("thirdtoken");
	}

	public void setThirdnickname(java.lang.String thirdnickname) {
		set("thirdnickname", thirdnickname);
	}

	public java.lang.String getThirdnickname() {
		return get("thirdnickname");
	}

	public void setThirdtype(java.lang.Integer thirdtype) {
		set("thirdtype", thirdtype);
	}

	public java.lang.Integer getThirdtype() {
		return get("thirdtype");
	}

	public void setImappkey(java.lang.String imappkey) {
		set("imappkey", imappkey);
	}

	public java.lang.String getImappkey() {
		return get("imappkey");
	}

	public void setImusername(java.lang.String imusername) {
		set("imusername", imusername);
	}

	public java.lang.String getImusername() {
		return get("imusername");
	}

	public void setImpassword(java.lang.String impassword) {
		set("impassword", impassword);
	}

	public java.lang.String getImpassword() {
		return get("impassword");
	}

}
