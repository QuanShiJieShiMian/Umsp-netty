package com.demo.admin;

import java.util.List;

import com.demo.common.model.User;
import com.demo.controls.BaseController;
import com.easemob.server.example.httpclient.apidemo.EasemobMessages;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * IndexController
 */
public class MsgPushController extends BaseController {
	public void index() {
		String msg = getPara("msg");
		if(isEmpty(msg)){
			renderErr("msg is null");
			return ;
		}
        List<User> resultList2 = User.dao.find(
				"select * from user ");
        if (resultList2==null||resultList2.size()<=0) {
        	renderErr("not found userlist");
			return ;
		}
        ObjectNode sendTxtMessageusernode = EasemobMessages.pushMsg(msg, resultList2);
        renderText("result:"+sendTxtMessageusernode);
	}
	public void pushAll() {
		index();
	}
	public void push() {
		String msg = getPara("msg");
		if(isEmpty(msg)){
			renderErr("msg is null");
			return ;
		}
		String username = getPara("username");
		if(isEmpty(username)){renderErr("username is null");return;}
		
        List<User> resultList2 = User.dao.find("select * from user where imusername = ?",username);
        if (resultList2==null||resultList2.size()<=0) {
        	renderErr("not found userlist");
			return ;
		}
        ObjectNode sendTxtMessageusernode = EasemobMessages.pushMsg(msg, resultList2);
        renderText("result:"+sendTxtMessageusernode);
	}
}