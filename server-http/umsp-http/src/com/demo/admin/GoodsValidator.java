package com.demo.admin;

import com.demo.common.model.Goods;
import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

/**
 * BlogValidator.
 */
public class GoodsValidator extends Validator {
	
	protected void validate(Controller controller) {
		validateRequiredString("goods.name", "nameMsg", "请输入标题!");
		validateRequiredString("goods.img", "imgMsg", "请输入图片路径!");
		validateInteger("goods.price", "priceMsg", "请输入价格,单位是分,正整数!");
		
		validateInteger("goods.isalive", "isaliveMsg", "上下架,0是上架,1是下架!正整数!");
		validateInteger("goods.fgoodstype", "fgoodstypeMsg", "分类,正整数!");
		System.out.println("validate ok");
	}
	
	protected void handleError(Controller controller) {
		controller.keepModel(Goods.class);
		System.out.println("validate have err");
		String actionKey = getActionKey();
		if (actionKey.equals("/admin/goods/save"))
			controller.render("add.html");
		else if (actionKey.equals("/admin/goods/update"))
			controller.render("edit.html");
	}
}
