package org.c.jni;


public class JniUtil {
    public static native void initJniEnvironment();
    public static native void callbackJniString(long pcb,int action,String data,int dataLen);
    public static native void test();
    
}
