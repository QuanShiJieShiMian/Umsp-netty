var HEART_BEAT_INTERVAL = 10000; //心跳间隔时间

//================== CMD =======================
function UmspCMD() {
    return UmspCMD.prototype;
}
var Umsp = new UmspCMD();
function UmspCMDMap(){
    return UmspCMDMap.prototype;
}
var CMDDescMap = new UmspCMDMap();
var CmdToString = function (cmd) {
    return CMDDescMap[cmd];
};

Umsp.HEARTBEAT = 520;
CMDDescMap[Umsp.HEARTBEAT]="HEARTBEAT";

Umsp.LOGIN = 1;
CMDDescMap[Umsp.LOGIN]="LOGIN";

Umsp.LOGIN_RSP = 2;
CMDDescMap[Umsp.LOGIN_RSP]="LOGIN_RSP";

Umsp.LOGOUT = 3;
CMDDescMap[Umsp.LOGOUT]="LOGOUT";

Umsp.LOGOUT_RSP = 4;
CMDDescMap[Umsp.LOGOUT_RSP]="LOGOUT_RSP";

Umsp.MSG = 5;
CMDDescMap[Umsp.MSG]="MSG";

Umsp.MSG_RSP = 6;
CMDDescMap[Umsp.MSG_RSP]="MSG_RSP";

Umsp.ROOM_SERVICE_LOGIN = 7;
CMDDescMap[Umsp.ROOM_SERVICE_LOGIN]="ROOM_SERVICE_LOGIN";

Umsp.ROOM_SERVICE_LOGIN_RSP = 8;
CMDDescMap[Umsp.ROOM_SERVICE_LOGIN_RSP]="ROOM_SERVICE_LOGIN_RSP";

Umsp.MATCH = 9;
CMDDescMap[Umsp.MATCH]="MATCH";

Umsp.MATCH_RSP= 10;
CMDDescMap[Umsp.MATCH_RSP]="MATCH_RSP";

Umsp.ROOM_ENTER= 11;
CMDDescMap[Umsp.ROOM_ENTER]="ROOM_ENTER";

Umsp.ROOM_EXIT= 12;
CMDDescMap[Umsp.ROOM_EXIT]="ROOM_EXIT";

Umsp.ROOM_USER_CHANGED= 13;
CMDDescMap[Umsp.ROOM_USER_CHANGED]="ROOM_USER_CHANGED";

Umsp.ERR = 999;
CMDDescMap[Umsp.ERR]="ERR";

//================== UmspCMD =======================
var FIXED_HEAD_SIZE = 24;

//================== Bean ======================
function User(userID, gameID, nickName) {
    this.userID = userID;
    this.gameID = gameID;
    this.token = "";
    this.nickName = nickName;
    this.matchExtInfo = "";
    this.accessFlag = 0;
}

function Match(matchType) {
    this.matchType = matchType;
    this.roomName = "";
    this.roomTag = "";
    this.matchServiceIndex = 0;
    this.wantToMatchUser = null;
    this.rollValue = 0;
    this.rollRang = 0;
    this.maxUserCount = 2;
}

Match.prototype.RANDOM = 1;
Match.prototype.SAME_NAME = 1 << 1;
Match.prototype.SAME_NAME_WITH_PASSWORD = 1 << 2;
Match.prototype.GROUP = 1 << 3;
