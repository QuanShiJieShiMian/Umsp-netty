### MatchVS-SDK-JS


#### 简介
MatchVSSDK的JavaScript版本

#### ChangedLog
1. init commit @2018/1/15 17:37 geliang

#### GetStart
1.搭建开发环境

```html

开发工具:WebStorm2017.3.2

调试工具:  
    1:chrome-Console
    2:IDEA-2017.3.2
    3.Netty4

三方依赖:
1.protobuf.js(BSB-3-clause)

JavaScrpit规范: 
ECMAScript5

参考:
    http://115.231.9.78 
    1:/matchvssdk/libmatchvs/blob/master/src/include/MatchvsResponse.h
    2:/matchvssdk/libmatchvs/blob/master/src/include/MatchVSDefines.h
    3:/matchvssdk/libmatchvs/edit/master/src/protocol/v1.4/stream.sdk.proto
    4:/matchvssdk/libmatchvs/blob/master/src/include/MatchVSEngine.h
```
2. 运行
   WebStorm -> run html on browse

## SDK文件合并

> 注意：生成Egret平台的 sdk需要修改 config.js 文件  
>
> MVS_PTF_ADATPER 值为 ENMU_MVS_PTF.MVS_EGRET
>
> 生成 Wechat平台的sdk 需要修改 config.js 文件
>
> MVS_PTF_ADATPER 值为 ENMU_MVS_PTF.MVS_COMMON 或者 ENMU_MVS_PTF.MVS_WX
>
> 普通 web版就设置 MVS_PTF_ADATPER 值为 ENMU_MVS_PTF.MVS_COMMON

运行脚本 package.sh脚本

```
#生成普通web平台sdk
./package.sh

#生成微信小游戏端sdk
./package.sh --weixin  

#生成Egret支持的sdk
./package.sh --egret
```

