var ServerConfig = {
    LocalConf: {
        HOST_GATWAY_ADDR: "ws://192.168.8.104:12344/ws",
        GETHOSTLIST_URL: "http://192.168.8.104:8080/getHostList",
        REGISTER_USER_URL: "http://192.168.8.104:8080/user/regist"

    },
    Erdange: {
        HOST_GATWAY_ADDR: "ws://118.24.53.22:12344/ws",
        GETHOSTLIST_URL: "https://api.erdange.com/getHostList",
        REGISTER_USER_URL: "https://api.erdange.com/user/regist"
    },
    HeXianMaJiang: {
        HOST_GATWAY_ADDR: "ws://websocket.hexianmajiang.com:12344/ws",
        GETHOSTLIST_URL: "http://api.hexianmajiang.com/getHostList",
        REGISTER_USER_URL: "http://api.hexianmajiang.com/user/regist"
    },
    FlutterGo: {
        HOST_GATWAY_ADDR: "ws://118.24.53.22:12344/ws",
        GETHOSTLIST_URL: "http://fluttergo.com/getHostList",
        REGISTER_USER_URL: "http://fluttergo.com/user/regist"
    }
};

function getHostConfig(envir) {
    if (ServerConfig[envir] !== undefined) {
        return ServerConfig[envir];
    } else {
        console.warn("not found the envir config:`" +envir+"` ,use ServiceConfig.LocalConf");
        return ServerConfig.LocalConf;
    }
}
