/*
 *  umsp.cpp
 *  umsp
 *
 *  Created by 杨雪芹 on 16/8/15.
 *  Copyright © 2016年 geliang. All rights reserved.
 *
 */

#include <iostream>
#include "umsp.hpp"
#include "umspPriv.hpp"

void umsp::HelloWorld(const char * s)
{
	 umspPriv *theObj = new umspPriv;
	 theObj->HelloWorldPriv(s);
	 delete theObj;
};

void umspPriv::HelloWorldPriv(const char * s) 
{
	std::cout << s << std::endl;
};

