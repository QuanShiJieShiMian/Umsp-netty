package gl.java.network.transport.kcp.core.internal;

import java.util.Set;

/**

 */
public interface ReItrSet<E> extends Set<E>, ReItrCollection<E> {

}
