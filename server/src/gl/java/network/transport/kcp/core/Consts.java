package gl.java.network.transport.kcp.core;

import io.netty.util.internal.SystemPropertyUtil;

public class Consts {

    public static final int FIXED_RECV_BYTEBUF_ALLOCATE_SIZE = SystemPropertyUtil.getInt("tcp.udpRecvAllocateSize", 2048);

    public static final int CLOSE_WAIT_TIME = 5 * 1000;

}
