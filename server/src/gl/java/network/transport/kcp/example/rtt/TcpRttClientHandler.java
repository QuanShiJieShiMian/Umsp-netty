package gl.java.network.transport.kcp.example.rtt;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**

 */
public class TcpRttClientHandler extends KcpRttClientHandler {

    private static final Logger log = LoggerFactory.getLogger(TcpRttClientHandler.class);


    /**
     * Creates a client-side handler.
     */
    public TcpRttClientHandler(int count) {
        super(count);
    }

    @Override
    public void channelActive(final ChannelHandlerContext ctx) {
        super.channelActive(ctx);
        this.w.setTitle("Tcp");
    }


    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
    }

    @Override
    public void channelRead(final ChannelHandlerContext ctx, Object msg) {
        super.channelRead(ctx,msg);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        super.exceptionCaught(ctx, cause);
    }

}
