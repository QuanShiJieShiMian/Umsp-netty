package gl.java.util;

import gl.java.umsp.UmspHeader;
import io.netty.buffer.ByteBufUtil;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.*;

@Slf4j
public class JavaTest {

    public static class A {
        public int a = 10;


        public void print() {
            System.out.println("super " + a);
        }

        protected void c() {
            System.out.println("ccc");
        }
    }

    public static class B extends A {
        int a = 100;

        public void printb() {
            System.out.println("child " + a);
        }

        @Override
        public void c() {
            super.c();
        }
    }
    static volatile  int index = 0;
    public static void main(String[] args) throws JSONException, InterruptedException {

        ExecutorService ex =  ThreadUtil.createSingleBlockThread(1024);
        int i = 0;
        try {
            for (; ;) {

                ex.execute(new Runnable() {
                    @Override
                    public void run() {
                        log.info("run:"+(index++));
                        try {
                            Thread.sleep(1);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
                log.info("execute :{}",i++);
            }
        } catch (Exception e) {
            e.printStackTrace();

            Thread.sleep(10000);

            ex.execute(new Runnable() {
                @Override
                public void run() {
                    log.info("run:"+(index++));
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

    }

    static int id = 0;

    public static void addNode(JSONArray jsonArray, int parent, int id) throws JSONException {
        JSONObject value = new JSONObject();
        value.put("parent", parent);
        value.put("id", id);
        jsonArray.put(value);
        System.out.println("parend" + parent + " id:" + id);
    }

    public static long GetMD5LongHash(String inputValue) {

        byte[] s;
        try {
            s = MessageDigest.getInstance("MD5").digest(inputValue.getBytes("UTF-8"));
            long result = 0;
            for (int i = 0; i < s.length; i += 2) {
                result <<= 8;
                result |= (s[i] ^ s[i + 1]);
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

}
