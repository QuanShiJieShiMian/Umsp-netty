package gl.java.util;

import gl.java.umsp.websocket.HttpRequestHandler;
import lombok.Cleanup;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/**
 * 配置文件读取工具类
 */
@Slf4j
public class PropertiesUtil {
    /**
     * 从标准properties文件(位于src目录下的*.properties)中载入配置信息(UTF-8 无Bom格式),如config.properties
     *
     * @param propertiesFileName 文件名
     * @return 配置
     */
    public static Properties loadProperties(String propertiesFileName) {
        Properties prop = new Properties();
        try {
//            log.info("class loader :" + PropertiesUtil.class.getResourceAsStream("/" + propertiesFileName));
//            log.info("class loader path: " + propertiesFileName);
            @Cleanup InputStreamReader in = new InputStreamReader(getResourceAsSteam(propertiesFileName), "UTF-8");
//            @Cleanup InputStreamReader in = new InputStreamReader(PropertiesUtil.class.getClassLoader().getResourceAsStream(File.separator + propertiesFileName), "UTF-8");
            prop.load(in);     //加载属性列表.
            in.close();
            log.info("load config.prop ,RoomIP:"+prop.getProperty("RoomIP"));
        } catch (Exception e) {
            e.printStackTrace();
            log.warn(e.getCause()+"");
        }
        return prop;
    }

    /**
     * 保存配置到System.Property中. 方便从System.getProperty(key)读取
     *
     * @param prop 要保存的属性
     */
    public static void saveToSystemProperty(Properties prop) {
        for (String key : prop.stringPropertyNames()) {
            String value = prop.getProperty(key);
            System.setProperty(key, value);
        }
//        Properties pros = System.getProperties();
//        pros.list(System.out);

    }

    public static void loadAndSave(String proFileName) {
        saveToSystemProperty(loadProperties(proFileName));
    }

    /**
     * 如果以jar形式运行,请将`资源文件`放在jar同级目录下
     * @param proFileName
     * @return
     * @throws URISyntaxException
     */
    public static InputStream getResourceAsSteam(String proFileName) throws Exception {
        URL location = HttpRequestHandler.class.getProtectionDomain()
                .getCodeSource().getLocation();

        String resPath = location.toURI().toString();
        resPath = !resPath.contains("file:") ? resPath : resPath.substring(5);
        if (resPath.toString().endsWith(".jar")) {
            log.info("getResourcesFromJar:" + resPath);
            return readZipFileAsSteam(resPath,proFileName);
        }
        String path = resPath + proFileName;
        log.info("getResourcesFromClassPath:" + path);

        return new FileInputStream(new File(path));
    }

    public static InputStream readZipFileAsSteam(String zipFile, String exactFile) throws Exception {
        ZipFile zf = new ZipFile(zipFile);
        InputStream in = new BufferedInputStream(new FileInputStream(zipFile));
        ZipInputStream zin = new ZipInputStream(in);
        ZipEntry ze;
        while ((ze = zin.getNextEntry()) != null) {
            if (ze.isDirectory()) {
            } else {
                System.err.println("zipFile - " + ze.getName() + " : "
                        + ze.getSize() + " bytes");

                if (exactFile.equals(ze.getName())){
                    InputStream inputStream = zf.getInputStream(ze);
                    zin.close();
                    return inputStream;
                }
            }
        }
        zin.closeEntry();
        return null;
    }
}


