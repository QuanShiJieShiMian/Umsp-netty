package gl.java.util;

public class JVM {
    public static void hookJVMShutDown(Runnable runnable) {
        Runtime.getRuntime().addShutdownHook(new Thread(runnable));
    }
}
