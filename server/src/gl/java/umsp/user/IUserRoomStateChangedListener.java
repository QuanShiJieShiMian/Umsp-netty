package gl.java.umsp.user;

import gl.java.umsp.bean.Room;
import gl.java.umsp.match.IUserConnectListener;

/**
 *  监听用户进出房间服务器
 */
public interface IUserRoomStateChangedListener extends IUserConnectListener{
    /**
     * //用户离开房间
     * @param userID 用户ID
     * @param roomID 房间ID
     */
    void onUserLeave(int userID, String roomID);

    /**
     * 用户进入房间
     * @param userID 用户ID
     * @param roomID 房间ID
     */
    void onUserEnter(int userID, String roomID);
}
