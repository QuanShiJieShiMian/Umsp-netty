package gl.java.umsp;

public enum ErrCode {

    NotLogin(201, "用户没登录"),

    NotSupportMatchType(405, "不支持的匹配方式"),

    RoomNotEnough(602, "没有足够的房间"),
    RoomOrUserInvalid(603, "匹配失败,用户或者房间信息被修改已无效,请重试"),
    RoomSerViceOffline(604, "房间服务不在线"),
    UserMatchIsFrequency(605, "匹配太频繁,请稍后重试"),
    UserHasInRoom(606, "用户已经在房间中"),
    UserIsOffline(607, "用户已经从GateWay离线"),


    ServiceBusy(503, "服务器忙,请稍后重试"),
    MatchNoHandler(510, "没有匹配Handler"),
    ;


    private final String describe;
    private final int code;

    ErrCode(int code, String describe) {
        this.code = code;
        this.describe = describe;
    }

    public String getDescribe() {
        return describe;
    }

    public int getCode() {
        return code;
    }


    @Override
    public String toString() {
        return "[+" + code + "] Error:" + describe;
    }

    public static String toString(int code) {
        for (ErrCode errCode : ErrCode.values()) {
            if (errCode.getCode() == code) {
                return "[+" + code + "] Error:" + errCode.describe;
            }
        }
        return "[" + code + "] : unknown errCode ";
    }

}
