package gl.java.umsp;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import lombok.extern.slf4j.Slf4j;

import java.util.List;


/**
 * 解码实际内容
 * 
 * @author geliang
 * 
 */
@Slf4j
public class UmspMessageDecoder extends ByteToMessageDecoder {

    public UmspMessageDecoder() {
    }

    // public static AtomicInteger count = new AtomicInteger(0)
    // public AtomicLong lastTime = new AtomicLong(0);
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in,
	    List<Object> out) throws Exception {
	int canReadByteLen = in.readableBytes();
	if (canReadByteLen <= 0) {
	    // Log.i("Close the " + ctx.channel().remoteAddress()
	    // + ",Not recevice any bytes from it");
	    in.release();
	    return;
	}

	UmspHeader message = UmspHeader.readMessage(in);

	out.add(message);
//    logger.debug("recv an umsp msg:"+message);
	// count.incrementAndGet();
	// long inval = System.currentTimeMillis()-lastTime.longValue();
	// if (inval>1000) {
	// logger.info(count.intValue()/(inval/1000f)+" -  [RecvMsgCount/Second]");
	// lastTime.set(System.currentTimeMillis()) ;
	// count.set(0);
	// }

    }

}
