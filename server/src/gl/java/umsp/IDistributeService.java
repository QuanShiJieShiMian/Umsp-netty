package gl.java.umsp;

import gl.java.util.JsonUtil;
import sun.security.krb5.Config;

/**
 * 定义一个分布式服务的编程范式.
 */
public interface IDistributeService {
    /**
     *服务的配置
     */
    public abstract class Config
    {
//        public abstract int getServiceIndex();

        public static <T> T fromJson(String json ,Class<T> clazz){
            return JsonUtil.fromJson(json,clazz);
        }
        public String toString(){
            return JsonUtil.toString(this);
        }

    }
    /**
     * 服务启动
     * @param config
     */
    void start(Config config);

    /**
     * 服务停止
     */
    void stop();


}
