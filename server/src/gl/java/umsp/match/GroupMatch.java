package gl.java.umsp.match;

import gl.java.pattern.NotImplementException;
import gl.java.umsp.bean.Match;

/**
 * 组队匹配
 * @author geliang
 *
 */
public class GroupMatch implements IMatch {

	@Override
	public void match(Match match, IMatchedCallBack listener) {
		// 优先获取群组匹配信息
		throw new NotImplementException();
	}

	@Override
	public int getMatchType() {
		
		return MatchType.GROUP;
	}

}
