package gl.java.umsp.match;

import gl.java.umsp.bean.Match;
import gl.java.umsp.bean.Room;

import java.util.List;

/**
 * 匹配接口,多线程匹配需保证线程安全
 *
 * @author geliang
 */
public interface IMatch {
    /**
     * @param match
     * @param listener 监听匹配成功的消息
     */
    public void match(Match match, IMatchedCallBack listener);

    /**
     * @return 匹配的方式  {@link gl.java.umsp.match.MatchType}
     */
    public int getMatchType();

    /**
     * 从已有房间中找一个符合条件的房间
     */
    public interface IRoomMatchFilter {
        /**
         * 过滤
         * @param rooms 人未满的房间
         * @param match 匹配条件
         * @return
         */
        Room filter(List<Room> rooms, Match match) ;
    }
}
