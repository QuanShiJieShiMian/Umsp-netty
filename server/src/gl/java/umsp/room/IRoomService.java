package gl.java.umsp.room;

import gl.java.umsp.IDistributeService;
import gl.java.umsp.UmspHeader;
import gl.java.umsp.bean.Room;
import gl.java.umsp.bean.User;

public interface IRoomService extends IDistributeService {
   boolean onUserEnter(Room room,User user); //rpc
    boolean onUserExit(Room room,User user); //rpc
    boolean onUserSendMsg(UmspHeader msg,Room room); //rpc
}
