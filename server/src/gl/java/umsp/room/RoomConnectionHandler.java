package gl.java.umsp.room;

import gl.java.umsp.bean.User;
import gl.java.umsp.event.Event;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RoomConnectionHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        super.userEventTriggered(ctx, evt);
        log.info(" user event triggered:"+evt);
        if (evt instanceof Event){
            Event e = (Event)evt;
            if (Event.USER_EXIT_ROOM.equals(e.eventName)) {
                RoomService.INSTANCE.onUserExit(null,new User((Integer) e.eventValue));
            }
        }
    }
}
