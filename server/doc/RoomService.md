[TOC]
# 房间服务器的设计

- @autor geliang
- @date 20170924 v1.0 初始设计
- @date 20170925 v1.1 增加RoomService的GateWay转发,使得RoomService在local也可以调试

### 接口
    接口设计满足 `转发用户消息` 的需求同时,满足下列原则;
    
    1)可靠性:
        房间服务器一个24小时运行的,有状态的服务,为了支持处理用户断线的异常情况:
        1. 心跳机制(空包头即可)
        2. 监听外部的用户掉线通知.(通常不是那么可靠)
        3. 数据持久化(redis)
        
    2)安全性:
        用户第一次登录调用客户端Enter函数,发起登录检验(可选),开启会牺牲一个hash查找的性能;
        
    3)拓展性:
        设计3个函数,可Hook一些用户行为 1)用户进入 2)用户离开 3)用户发消息
        
    接口 `IRoomService` 设计如下:        
        1. start
        2. stop
        3. onUserEnter(int gameID,int roomID,int UserID,String session) //rpc,hook
        4. onUserExit() //rpc,hook
        5. onUserSendMsg(int userID,int roomID,byte[] data,int dataLen); //rpc,hook
    
### 流程
```text
1. start
    1.1 如果没有则注册房间列表
    1.2 设置服务可用 ,发RoomServiceStart广播
    1.3 监听jvm ShutDown事件,调用stop设置服务不可用
2. 监听消息
    2.1 处理 `onUserEnter` 事件, 检验用户合法性
    2.2 处理 `onUserExit` 事件, 清除用户信息
    2.2 处理 `onUserSendMsg` 事件,转发消息 
3. stop
    1.2: 设置服务不可用,发RoomServiceStop广播
```


### 数据结构
- RoomServer:
```json
    {
      "GameName": "斗地主",
      "RoomServiceName": "一区",
      "GameID": 0,
      "RoomAreaIndex": 0,
      "MAX_USER_ON_SERVICE": 65536,
      "MaxRoom": 16384,
      "MaxUserPerRoom": 4,
      "RoomIDBaseIndex": 0,
      "IP": "未指定的外网IP",
      "PORT": "未指定的外网端口",
      "IsProxy": true,
      "RunState": true
     }
```

- Room:
```json
    {
      "roomID": 0,
      "roomIP": 0,
      "roomPort": 0,
      "gameID": 200661,
      "maxRoomUser": 2,
      "roomState": 0,
      "roomUserList": [],
      "delayForRemoveOfflineUser": 0
    }
```
### 安全校验算法
    String session = des("key", gameID+@+roomID+@+UserID);
    key配置在RoomServiceConfig放在Redis中

### RoomService的GateWay转发
    使得RoomService在本地也可以调试,原理:
    1. RoomService在启动时额外启动一个连接,连向GateWay,同时发送roomlogin指令
    2. GateWay在接收到用户消失时通过这个连接将消息转发给GateWay
    3. Room需定期向GateWay发送心跳
    RoomService无法获取用户数据.
    ps: 这种方式只能有限开放,存在较大安全隐患,必须采取一定授权失效机制.如token过期