[TOC]

- @autor geliang
- @date 20171019 v1.0 

## EventFlow 事件流
    此文档主要讲述服务对来自内部(服务调度)/外部(Client)的各种行为事件的处理流程.阐述其处理方式的设计优缺点
### 名词解释
    - 广播
        分布式服务间通过订阅/发布机制给订阅者发送消息的一种手段.
        v1.0使用Redis实现订阅发布机制
    - 事件/Event
        在分布式服务间传递的消息/变化统称为Event
### 事件      
#### Online/Offline/Login
    Connect/Disconnect事件的处理由GateWay实现
    1: EventUserOnline
        Client与GateWay建立TCP链接,保存用户链接状态.GateWay广播EventUserOnline事件
    2: EventLogin
        Client上报Login指令.GateWay转发给Auth服务.Auth上报Result给GateWay.GateWay根据Result选择是否断开用户连接
        ps:v1.0的版本GateWay兼职Auth的职责
    3: EventUserOffline
        Client主动/异常断开TCP链接,GateWay广播EventUserOffline时间.
           
#### Match
    Match事件的处理由MatchService实现.
    1: EvnetMatch
       Client发送Event给GateWay,GateWay广播EventMatch,MatchService会订阅此消息.分配房间,并更新玩家的RoomState.
    2: EventMatchResult
       MatchService在匹配完成后会发送EventMatchResult,GateWay收到此消息.给Client下发EventMatchResult
  
#### Enter/Exit Room
    Enter/ExitRoom事件的处理由RoomService实现
    1: EventUserEnterRoom 
       Client与RoomService建立Tcp连接,RoomService发出EventUserEnterRoom给Match
       更新User的RoomState
    2: EventUserExitRoom 
       Client与RoomService断开Tcp连接或者上报UserExitRoom指令,RoomService发出EventUserExitRoom给Match
       更新User的RoomState
    ps: 因为房间的分配是由MatchService进行的.所以房间的回收(UserExitRoom事件触发)也应该由MatchService进行,
        以保持系统的无锁化和低耦合.避免在分布式系统中引入分布式锁等复杂易出错的技术.
       
