nohup java -jar GateWay.jar 12345 &
nohup java -jar GateWay.jar 12345 >/dev/null 2>&1 &

启动zookeeper和kafka
nohup bin/zookeeper-server-start.sh config/zookeeper.properties >/dev/null 2>&1 &
nohup bin/kafka-server-start.sh config/server.properties >/dev/null 2>&1 &

查看队列中的消息
bin/kafka-console-consumer.sh --bootstrap-server api.hexianmajiang.com:9092 --topic test --from-beginning

创建广播主题
bin/kafka-topics.sh --create --zookeeper api.hexianmajiang.com:2181 --replication-factor 1 --partitions 1 --topic test
查询主题列表
bin/kafka-topics.sh --list --zookeeper api.hexianmajiang.com:2181

给主题发消息
bin/kafka-console-producer.sh --broker-list api.hexianmajiang.com:9092 --topic test

消费消息
bin/kafka-console-consumer.sh --bootstrap-server api.hexianmajiang.com:9092 --topic test --from-beginning
