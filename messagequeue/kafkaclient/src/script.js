var fun1 = function(name) {
    print('Hi there from Javascript, ' + name);
    return "greetings from javascript";
};

var fun2 = function (object) {
    print("JS Class Definition: " + Object.prototype.toString.call(object));
    print("JS object : " + object.name);
    print("JS object : " + object.age);
};

var Main = Java.type('JavaScriptEngine');

var result = Main.log('John Doe');
print(result);