package kfk;

public interface IEventSubscriber {
    public void onMessage(String channel, String key);
}
