﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public class SimpleButton : MonoBehaviour{
	// Use this for initialization
	void Start (){

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void onMsg(string msg){

    }

    private int index = 0;
    public void OnClick(Object o){
        Log.i("onclick:"+o.name );
        switch(o.name){
            case "BtnStart":
                GameObject.Find("Canvas").GetComponent<Demo>()?.startMatch();
                break;
            case "BtnStop":
                GameObject.Find("Canvas").GetComponent<Demo>()?.stopMatch();
                break;
            case "BtnSend":
                GameObject.Find("Canvas").GetComponent<Demo>()?.send("hello 中文测试! at:"+index++);
                break;
        }
    }
}
